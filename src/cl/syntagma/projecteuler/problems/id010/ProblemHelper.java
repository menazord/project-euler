package cl.syntagma.projecteuler.problems.id010;

import cl.syntagma.projecteuler.problems.base.AbstractHelper;
import cl.syntagma.projecteuler.utils.MathUtils;



/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * 
 * Find the sum of all the primes below two million.
 * 
 * 
 * @author cristian
 *
 */
public class ProblemHelper extends AbstractHelper{
	
	public ProblemHelper() {
 
	}
	
	public long findPrimeSum(long until) {

		int n = 3;
		long sum = 2;
		
		for ( ; n < until; n+= 2){
			
			if (MathUtils.isPrime(n))
				sum += n;
			
		}
		
		return sum;
	}

	public static void main(String[] args) {

		log("Started");
		ProblemHelper helper = new ProblemHelper();
		long product = helper.findPrimeSum(2000000);
		
		log("Sum is = " + product);
		log("Done.");
	}

}
