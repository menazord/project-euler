package cl.syntagma.projecteuler.problems.id015;

import cl.syntagma.projecteuler.problems.base.AbstractHelper;
import cl.syntagma.projecteuler.utils.GridUtils;



/**
 * Starting in the top left corner of a 2�2 grid, 
 * and only being able to move to the right and down, 
 * there are exactly 6 routes to the bottom right corner.
 * 
 * How many such routes are there through a 20�20 grid?
 * 
 * @author cristian
 *
 */
public class ProblemHelper extends AbstractHelper{

	public ProblemHelper() {
 
	}
	
	public long getMaxRoutes(int gridSize) {

		long routesQty = GridUtils.getLatticePaths(gridSize);
				
		return routesQty;
	}

	public static void main(String[] args) {

		log("Started");
		ProblemHelper helper = new ProblemHelper();
		long routes = helper.getMaxRoutes(20);
		
		log("Max routes are = " + routes);
		log("Done.");
		
	}

}
