package cl.syntagma.projecteuler.problems.id004;

import cl.syntagma.projecteuler.utils.MathUtils;

/**
 * A palindromic number reads the same both ways. 
 * The largest palindrome made from the product of 
 * two 2-digit numbers is 9009 = 91 � 99.
 * 
 * Find the largest palindrome made from the product 
 * of two 3-digit numbers.
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	public ProblemHelper() {
 
	}
	
	public long getLargestPalindrome(long a, long b) {
		
		long largest = 0;
		
		for (long xa = a; xa > 0; xa--){
			for (long xb = b ; xb > 0; xb--){
				
				long trial = xa * xb;
				if (MathUtils.isPalindrome(trial)){
					if (trial > largest)
						largest = trial;
				}
			}
		}
		
		return largest;		
	}

	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long largestPalindrome = helper.getLargestPalindrome(999, 999);
		
		System.out.println("Result is : " + largestPalindrome);
		System.out.println("Done.");
	}

}
