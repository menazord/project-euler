package cl.syntagma.projecteuler.problems.id002;

import cl.syntagma.projecteuler.utils.MathUtils;

/**
 * Each new term in the Fibonacci sequence is generated 
 * by adding the previous two terms. By starting with 1 and 2, 
 * the first 10 terms will be:
 * 
 *       1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 *       
 * By considering the terms in the Fibonacci sequence 
 * whose values do not exceed four million, find the sum 
 * of the even-valued terms.
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	private static final long MAX = 4000000;
	
	private long sum;
	
	public ProblemHelper() {
		sum = 0;
	}
	
	public long getSum() {
		
		boolean found = false;
		long f1 = 1;
		long f2 = 1;
		long current = 0;
		
		while (!found){
			
			current = MathUtils.getNextFibonacci(f1, f2);
			
			if (current > MAX)
				break;
			
			if (current % 2 == 0)
				sum += current;			
			
			f1 = f2;
			f2 = current;
		}

		return sum;		
	}
	
	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long sum = helper.getSum();
		
		System.out.println("Result is : " + sum);
		System.out.println("Done.");
	}
}
