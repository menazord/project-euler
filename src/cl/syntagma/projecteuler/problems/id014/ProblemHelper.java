package cl.syntagma.projecteuler.problems.id014;

import java.util.List;

import cl.syntagma.projecteuler.problems.base.AbstractHelper;
import cl.syntagma.projecteuler.utils.MathUtils;



/**
 * The following iterative sequence is defined for the 
 * set of positive integers:
 * 
 * n --> n/2 (n is even)
 * n --> 3n + 1 (n is odd)
 * 
 * Using the rule above and starting with 13, we generate 
 * the following sequence:
 * 
 * 13 --> 40 --> 20 --> 10 --> 5 --> 16 --> 8 --> 4 --> 2 --> 1
 * 
 * It can be seen that this sequence (starting at 13 and 
 * finishing at 1) contains 10 terms. Although it has not 
 * been proved yet (Collatz Problem), it is thought that all 
 * starting numbers finish at 1.
 * 
 * Which starting number, under one million, produces the 
 * longest chain?
 * 
 * NOTE: Once the chain starts the terms are allowed to go 
 * above one million.
 * 
 * @author cristian
 *
 */
public class ProblemHelper extends AbstractHelper{

	public ProblemHelper() {
 
	}
	
	public long getLargestCollatz(int start) {

		long thisChain = 0;
		long maxChainStart = 0;
		long maxChain = 0;
		
		for (int i = start; i > 1; i--){
			List<Long> chain = MathUtils.getCollatzChain(i); 
			thisChain = chain.size();
			if (thisChain > maxChain){
				maxChain = thisChain;
				maxChainStart = chain.get(0);
			}
		}
				
		return maxChainStart;
	}

	public static void main(String[] args) {

		log("Started");
		ProblemHelper helper = new ProblemHelper();
		long maxChain = helper.getLargestCollatz(999999);
		
		log("Number is = " + maxChain);
		log("Done.");
		
	}

}
