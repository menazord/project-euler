package cl.syntagma.projecteuler.problems.id017;

import cl.syntagma.projecteuler.problems.base.AbstractHelper;
import cl.syntagma.projecteuler.utils.NumberToWords;



/**
 * If the numbers 1 to 5 are written out in words: 
 * one, two, three, four, five, 
 * then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * 
 * If all the numbers from 1 to 1000 (one thousand) inclusive 
 * were written out in words, how many letters would be used?
 * 
 * NOTE: Do not count spaces or hyphens. For example, 
 * 342 (three hundred and forty-two) contains 23 letters 
 * and 115 (one hundred and fifteen) contains 20 letters. 
 * The use of "and" when writing out numbers is in compliance 
 * with British usage.
 *  
 * @author cristian
 *
 */
public class ProblemHelper extends AbstractHelper{

	public ProblemHelper() {
 
	}
	
	public double getSpellingSum(int until) {

		long bigAssSum = 0;
		
		for (int i = 1; i <= until; i++){
			String word = NumberToWords.ConvertNumberToText(i).trim();
			log("Num : " + i + " , Word : " + word + " , len : " + word.length());
			bigAssSum += word.length();
		}
		
		System.out.println();
		
		return bigAssSum;
	}

	public static void main(String[] args) {

		log("Started");
		ProblemHelper helper = new ProblemHelper();
		double sum = helper.getSpellingSum(1000);
		
		log("Sum is = " + plain(sum));
		log("Done.");
		
	}

}
