package cl.syntagma.projecteuler.problems.id001;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, 
 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * 
 * Find the sum of all the multiples of 3 or 5 below 1000.
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	private static final int MAX = 1000;
	
	private long sum;
	
	public ProblemHelper() {
		sum = 0;
	}
	
	public long getSum() {
		
		for (int i = 0; i < MAX; i++){
			
			if ( ( i % 3 == 0 ) || (i % 5 == 0) )
				sum += i;
		}

		return sum;		
	}
	
	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long sum = helper.getSum();
		
		System.out.println("Result is : " + sum);
		System.out.println("Done.");
	}
}
