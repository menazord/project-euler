package cl.syntagma.projecteuler.problems.base;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class AbstractHelper {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");  

	private static DecimalFormat df = new DecimalFormat("#");
	
	protected static void log(String msg){
		System.out.println(
				sdf.format(new Timestamp(System.currentTimeMillis()))
				+ " - "
				+ msg);
	}
	
	
	protected static String plain(double d){
		return df.format(d);
	}
}
