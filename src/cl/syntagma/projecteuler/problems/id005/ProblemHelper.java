package cl.syntagma.projecteuler.problems.id005;


/**
 * 2520 is the smallest number that can be divided 
 * by each of the numbers from 1 to 10 without any remainder.
 * 
 * What is the smallest positive number that is evenly 
 * divisible by all of the numbers from 1 to 20?
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	public ProblemHelper() {
 
	}
	
	public long getSmallestNumber(int until) {
		
		boolean found = false;
		long smallest = 0;
		
		int count = 0;
		for (long i = 1; !found ; i++){
			count = 0;
			for (int j = 1; j <= until; j++){
				if (i % j == 0)
					count ++;
				else
					break;
				
				if (count == until){
					smallest = i;
					found = true;
					break;
				}
					
			}
			
		}
		
		return smallest;		
	}

	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long smallestNumber = helper.getSmallestNumber(20);
		
		System.out.println("Result is : " + smallestNumber);
		System.out.println("Done.");
	}

}
