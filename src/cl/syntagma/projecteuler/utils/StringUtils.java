package cl.syntagma.projecteuler.utils;


public class StringUtils {

	
	public static boolean isPalindrome (String str){
		
		StringBuilder sb = new StringBuilder(str);
		return sb.reverse().toString().equals(str);
		
	}
	
}
