package cl.syntagma.projecteuler.problems.id006;


/**
 * The sum of the squares of the first ten natural 
 * numbers is,
 * 
 * 1^2 + 2^2 + ... + 10^2 = 385
 * The square of the sum of the first ten natural 
 * numbers is,
 * 
 * (1 + 2 + ... + 10)^2 = 552 = 3025
 * 
 * Hence the difference between the sum of the squares 
 * of the first ten natural numbers and the square of 
 * the sum is 3025 - 385 = 2640.
 * 
 * Find the difference between the sum of the squares 
 * of the first one hundred natural numbers and the square 
 * of the sum.
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	public ProblemHelper() {
 
	}
	
	public long getDifference(int until) {
		
		long sumA = 0;
		long sumB = 0;
		for (long i = 1; i <= until; i++){
			sumA += (i * i);
			sumB += i;
		}
		
		return (sumB * sumB) - sumA;		
	}

	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long difference = helper.getDifference(100);
		
		System.out.println("Result is : " + difference);
		System.out.println("Done.");
	}

}
