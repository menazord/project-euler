package cl.syntagma.projecteuler.problems.id016;

import java.math.BigDecimal;

import cl.syntagma.projecteuler.problems.base.AbstractHelper;



/**
 * 2^15 = 32768 and the sum of its digits 
 * is 3 + 2 + 7 + 6 + 8 = 26.
 * 
 * What is the sum of the digits of the 
 * number 2^1000? 
 *  
 * @author cristian
 *
 */
public class ProblemHelper extends AbstractHelper{

	public ProblemHelper() {
 
	}
	
	public double getResultSum(int base, int exp) {

		long bigAssSum = 0;
		
		BigDecimal bigAss = new BigDecimal(base).pow(exp);

		System.out.println(bigAss.toString());
		
		for (char ch : bigAss.toString().toCharArray()){
			bigAssSum += (ch - 48);
		}
		
		return bigAssSum;
	}

	public static void main(String[] args) {

		log("Started");
		ProblemHelper helper = new ProblemHelper();
		double sum = helper.getResultSum(2, 1000);
		
		log("Sum is = " + plain(sum));
		log("Done.");
		
	}

}
