package cl.syntagma.projecteuler.problems.id007;

import cl.syntagma.projecteuler.utils.MathUtils;


/**
 * By listing the first six prime numbers: 
 * 2, 3, 5, 7, 11, and 13, 
 * we can see that the 6th prime is 13.
 * 
 * What is the 10001st prime number?
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	public ProblemHelper() {
 
	}
	
	public long findPrime(int until) {

		int current = 6;
		long element = 0;
		for (long i = 14; ; i++){
			
			if (MathUtils.isPrime(i)){				
				current++;
				if (current == until){
					element = i;
					break;
				}
			}
		}
		
		return element;		
	}

	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long prime = helper.findPrime(10001);
		
		System.out.println("Result is : " + prime);
		System.out.println("Done.");
	}

}
