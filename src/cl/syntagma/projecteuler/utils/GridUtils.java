package cl.syntagma.projecteuler.utils;

public class GridUtils {

	
	public static int[][] prepareGrid(String[] values, int m, int n){
		
		int [][] grid = new int[m][n];
		
		for (int i = 0; i < values.length; i++){
			
			grid[i] = rowPadding(n, 0);
			String []valueArray = values[i].split(" ");
			
			for (int j = 0; j < valueArray.length; j++){
				grid[i][j] = Integer.parseInt(valueArray[j]);
			}
		}
		
		return grid;
	}
	
	public static int[] rowPadding(int len, int paddingValue){
		
		int[] arr = new int[len];
		
		for (int i = 0; i < len; i++)
			arr[i] = paddingValue;
		
		return arr;
		
	}
	
	public static long getMaxHorizontalValueInGrid(int[][] grid, int m, int n, int sumLength){
	
		long maxVal = 0;		
		long currentVal = 0;
		
		for (int i = 0; i < m; i++){
			for (int j = 0; j < n + 1 - sumLength; j++){
				currentVal = 1;
				for (int z = 0; z < sumLength; z++)
					currentVal *= grid[i][j + z];
				if (currentVal > maxVal)
					maxVal = currentVal;
			}
		}
			
		return maxVal;
	}

	public static long getMaxVerticalValueInGrid(int[][] grid, int m, int n, int sumLength){
		
		long maxVal = 0;		
		long currentVal = 0;
		
		for (int i = 0; i < n + 1 - sumLength; i++){
			
			for (int j = 0; j < m + 1 - sumLength; j++){
				// search top left, bottom right
				currentVal = 1;				
				for (int x = 0, y = 0; x < sumLength && y < sumLength; x++, y++)
					currentVal *= grid[i + x][j + y];
				
				if (currentVal > maxVal)
					maxVal = currentVal;
				
				// search bottom left, top right
				currentVal = 1;				
				for (int x = sumLength - 1, y = 0; x >= 0 && y < sumLength; x--, y++)
					currentVal *= grid[i + x][j + y];
				
				if (currentVal > maxVal)
					maxVal = currentVal;				
			}
		}
			
		return maxVal;
	}

	public static long getMaxDiagonalValueInGrid(int[][] grid, int m, int n, int sumLength){
		
		long maxVal = 0;
		
		
		return maxVal;
	}

	
	public static long getLatticePaths(int gridSize){
		
		long paths = 0;
		
		double a = MathUtils.factorial(2 * gridSize);
		double b = Math.pow(MathUtils.factorial(gridSize), 2);
		
		paths =  (long)(a / b);
		
		return paths;
	}
	
	
}
