package cl.syntagma.projecteuler.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MathUtils {

	public static long getNextFibonacci(long f1, long f2){

		return f1 + f2;
		
	}
		
	public static Map<Long, Long> factorize (long n){

		long number = n;
		Map<Long, Long> factors = new HashMap<Long, Long>();
		
		if (isPrime(n)){
			factors.put(new Long(n), new Long(1));
			return factors;
		}
		
		for (int i = 2; i < number / 2; i ++){
			
			while (number % i == 0){
				
				// Assume is the first time
				Long factor = new Long(1);
				
				if (factors.containsKey(new Long(i))){
					factor = factors.get(new Long(i)) + 1;
				}
				
				factors.put(new Long(i), factor);

				number /= i;
			}
			
		}
		
		if (number > 1)
			factors.put(new Long(number), new Long(1));
		
		return factors;
	}
	
	public static long divisors (long n){
		
		long dividers = 1;
		Map<Long, Long> factors = factorize(n);
		
		for (Long factor : factors.keySet()){
			Long exp = factors.get(factor);
			dividers *= (exp + 1);
		}
		
		return dividers;
	}
	
	public static long triangularNumber(long until){
		
		long num = 0;
		
		num = (until * (until + 1)) / 2;
		
		return num;
	}
	
	public static List<Long> getCollatzChain(long n){
		
		long remain = n;
		List<Long> chain = new ArrayList<Long>();
		while (remain > 0){

			if (remain == 1){
				chain.add(new Long(1));
				break;
			}else
				chain.add(new Long(remain));
				
			if (remain % 2 == 0){
				remain /= 2;
			}else{
				remain = (remain * 3) + 1;
			}
		}
		
		return chain;
		
	}
	
	public static boolean isPrime(long n){

		if (n == 2)
			return true;
		else if (n % 2 == 0)
			return false;
		else if (n % 3 == 0 && n > 3)
			return false;
		
	    double sRoot = Math.sqrt(n * 1.0);
		
		for (int i = 3; i <= sRoot; i++){
			
			if (n % i == 0 && i != n){
				
				return false;
			}
			
		}
		
		return true;
	}


	public static double factorial(long n){
		
		double fact = 1;
		
		for (long i = 1; i <= n; i++){
			fact *= i;
		}
		
		return fact;
	}
	
	public static boolean isPalindrome(long n){
		return StringUtils.isPalindrome("" + n);
	}
}
