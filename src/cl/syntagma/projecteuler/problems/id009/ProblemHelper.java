package cl.syntagma.projecteuler.problems.id009;



/**
 * A Pythagorean triplet is a set of three natural 
 * numbers, a < b < c, for which,
 * 
 * a2 + b2 = c2
 * 
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * 
 * There exists exactly one Pythagorean triplet for 
 * which a + b + c = 1000.
 * 
 * Find the product abc.
 * 
 * 
 * @author cristian
 *
 */
public class ProblemHelper {
	
	public ProblemHelper() {
 
	}
	
	public long findTripplet() {

		int a = 1;
		int b = 1;
		int c = 1;
		int sum = 0;
		
		for (a = 1; ; a++){
			
			for (b = a + 1; ; b++){
				
				sum = a + b + c;
				if (sum >= 1000)
					break;
				
				for (c = b + 1; ; c++){
					
					sum = a + b + c;
					
					if ( (a*a) + (b*b) == (c*c)){
						if (sum == 1000){
							System.out.println("Found tripplet : "
									+ "a(" + a + ") "
									+ "b(" + b + ") "
									+ "c(" + c + ")");
							return a * b * c;
						}						
					}else if (sum > 1000){
						c = 0;
						break;
					}					
				}
			}
			
		}
		
	}

	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long product = helper.findTripplet();
		
		System.out.println("Product is = " + product);
		System.out.println("Done.");
	}

}
