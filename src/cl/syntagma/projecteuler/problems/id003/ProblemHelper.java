package cl.syntagma.projecteuler.problems.id003;

import cl.syntagma.projecteuler.utils.MathUtils;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 * 
 * @author cristian
 *
 */
public class ProblemHelper {

	public ProblemHelper() {
 
	}
	
	public long getLargestFactor(long n) {
		
		long largest = 0;

		
		for (Long factor: MathUtils.factorize(n).keySet())
			if (factor > largest)
				largest = factor;
		
		return largest;		
	}

	public static void main(String[] args) {
		
		ProblemHelper helper = new ProblemHelper();
		long largestFactor = helper.getLargestFactor(600851475143L);
		
		System.out.println("Result is : " + largestFactor);
		System.out.println("Done.");
	}

}
